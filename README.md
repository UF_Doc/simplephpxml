# README #

This simple PHP code can be used to visually display an XML file in a browser. The intention is for viewing and modifying configuration files that are XML based.
The script will process any well formed XML without the need for a definition file. 

### What is this repository for? ###

* SimplePHPXML
* Version: 0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Required Improvements ###

* Add functionality to edit XML document and export/save
* Add undo buffer so that roll back is availible, this should track all actions performed on the XML
* Optional DTD etc to ensure xml conforms

