<!doctype html>
<html>
    <head>
        <title>SimplePHPXML</title>
        <link rel="stylesheet" type="text/css" href="./css/styles.css" />
    </head>
    <body>
        <div id="wrapper">
<?php
        
        function createheaderDIV ($tag) {
            //Include event functions here
            $tagname = $tag->getName();
            $tagchildren = $tag->count();
            echo '
            <div class="tagheader">
                    <div class="tagname"> <b>'.$tagname.'</b> </div>
                    <div class="childcount"> Children Nodes: <b>'.$tagchildren.'</b> </div>
                    <div class="tagattributes">';
                        foreach($tag->attributes() as $a => $b) {
                            echo $a,'="',$b,"\"   ";
                        }
            echo '  </div>';
            echo '  <div class="tagremove"> [-] </div> ';
            echo '  <div class="tagadd"> [+] </div> ' ;
            echo '            
            </div>';
        }
        
        function decomposeXML ($xml,$level) {
            $tagname = $xml->getName();
            
            $width = 100 - ($level*5);
            echo "<div class=\"tagcontainer\" style=\"width:$width%\"\">\n";
            createheaderDIV($xml);
            echo '<div class="tagbody">';
            echo '<div class="tagcontents">' . $xml . '</div> ';
            foreach ($xml->children() as $next_xml) {
                decomposeXML($next_xml,$level+1);
            }
            echo "</div></div>\n";
        }

        libxml_use_internal_errors(true);
        $xmlfilepath = "./xml/books.xml";
        $xml = simplexml_load_file($xmlfilepath); //load file as SimpleXML object
        if ($xml === false) {
            echo "<h1>Failed loading XML</h1>\n";
            foreach(libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
            exit ("Please check your input XML for compliance.");
        }

        
        decomposeXML($xml,0);
         
        //$new = fopen("./xml/newFile.xml", "w"); // open new file
        //fwrite($new, $newXml->asXML()); //write XML to new file using asXML method
        //fclose($new); // close the new file
?>
        </div>
    </body>
</html>